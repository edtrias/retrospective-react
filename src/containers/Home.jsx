import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as GreetingActions from 'store/actions/greetingActions';
import './styles.scss';

const propTypes = {
    greeting: PropTypes.string.isRequired,
    changeGreeting: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
    greeting: state.greeting.text
})

const mapDispatchToProps = (dispatch) => ({
    changeGreeting: bindActionCreators(GreetingActions, dispatch).changeGreeting
})

const Home = (props) => {
    const { greeting, changeGreeting } = props;
    const [ count, setCount ] = useState(0);

    useEffect(() => {
        document.title = `You clicked ${count} times`;
    });

    const handleClick = () => {
        changeGreeting();
        setCount(count + 1);
    }

    return (
        <div className="home-component">
            {greeting}
            <div className="button" onClick={handleClick}>
                <div className="button_content">
                    Change The Greeting
                </div>
            </div>
        </div>
    )
}

Home.propTypes = propTypes;

export default connect(mapStateToProps, mapDispatchToProps)(Home);