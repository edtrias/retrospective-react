import { TOGGLE_GREETING } from 'store/actions/greetingActions';

const initialState = { text: 'Hello World!' };

const greetingReducer = (state = initialState, action) => {
    switch (action.type) {
    case TOGGLE_GREETING:
        return {
            ...state,
            text: action.payload
        };
    default:
    };
    return state;
};

export default greetingReducer;